package ru.kuleshov.hometask.task2.vending;

import ru.kuleshov.hometask.task2.vending.drinks.Drink;
import ru.kuleshov.hometask.task2.vending.drinks.ColdDrink;
import ru.kuleshov.hometask.task2.vending.drinks.HotDrink;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Drink[] drinks = new Drink[]{
                new HotDrink("Чай", 150),
                new HotDrink("Кофе", 200),
                new ColdDrink("Кола", 50)
        };

        VendingMachine vm = new VendingMachine(drinks);
        vm.printMenu();

        Scanner in = new Scanner(System.in);
        for (;;) {
            System.out.print("Внесите сумму: ");
            double money = in.nextDouble();
            System.out.print("Выберите напиток: ");
            int key = in.nextInt();

            vm.giveMeADrink(key, money);
        }
    }
}