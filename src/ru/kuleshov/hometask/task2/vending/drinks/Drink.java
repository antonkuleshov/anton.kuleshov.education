package ru.kuleshov.hometask.task2.vending.drinks;

public interface Drink {
    double getPrice();
    String getTitle();
}