package ru.kuleshov.hometask.task2.vending;


import ru.kuleshov.hometask.task2.vending.drinks.Drink;
import java.util.Scanner;

public class VendingMachine {
    private Drink[] drinks;
    private double money;

    public VendingMachine(Drink[] drinks) {
        this.drinks = drinks;
    }

    private Drink getDrink(int key) {
        if (key < drinks.length){
            return drinks[key];
        } else {
            return null;
        }
    }

    public void giveMeADrink(int key, double money) {
        if (money > 0) {
            Drink drink = getDrink(key);

            if (drink != null) {
                if (drink.getPrice() <= money) {
                    System.out.println("Возьмите ваш напиток: " + drink.getTitle());
                    double delta = money - drink.getPrice();
                    if (delta != 0) {
                        System.out.println("Ваша сдача = " + delta);
                    }
                } else {
                    System.out.println("Недостаточно средств! " + (drink.getPrice() - money));
                }
            } else {
                System.out.println("Напиток не найден");//нет логики на отсутсвие напитка
            }
        } else {
            System.out.println("Бесплатно не работаем!");
        }
    }

    public void printMenu(){
        for (int i = 0; i < drinks.length; i++) {
            System.out.println(i + " " + drinks[i].getTitle() + " " +drinks[i].getPrice());
        }
    }


}
