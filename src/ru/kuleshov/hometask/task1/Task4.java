package ru.kuleshov.hometask.task1;

import java.util.Scanner;
public class Task4 {
    public static void main(String[] args) {
       long factorial = 1; //нужно зарезолвить кейс, когда в long не помещается значение (больше 20)
        System.out.println("Введите целое положительное число для нахождения факториала: ");
       Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        for (int i = 1; i <= n; i++) {
            factorial = factorial*i;
        }
        System.out.printf(n+"! = " + "%,d", factorial);
    }
}
