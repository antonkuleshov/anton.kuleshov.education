package ru.kuleshov.hometask.task1;

import java.util.Scanner;
public class Task3 {
    public static void main(String[] args) {
        Scanner scanX = new Scanner(System.in);
        Scanner scanY = new Scanner(System.in);
        System.out.print("Введите порог по оси X: ");
        int x = scanX.nextInt();
        System.out.print("Введите порог по оси Y: ");
        int y = scanY.nextInt();
        int multiplicationTable[][] = new int[10][10];

        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                multiplicationTable[i][j]=(i+1)*(j+1);
                System.out.printf("%4d", multiplicationTable[i][j]);
            }
            System.out.println();
        }
    }
}
